# Kafka Consumer Elasticsearch

A micro service to consume messages from kafka topics and index them 
in an elasticsearch cluster. 

# Configuration

## kafka.brokers 

A list of comma separated kafka brokers given as host:port pairs.

## kafka.consumer.group.id

The consumer group id. Should be shared with all other consumers that read from 
the same topics where messages should not be duplicated!

## kafka.consumer.topics
   
A comma separated list of all topics this consumer should subscribe to. 

## kafka.producer.topics

This property is used to specify retry topics for each subscribed topic. 
When a message is indexed and a version number is given as metadata elasticsearch
will check the version number of the existing document. If it is the same 
the existing document is overwritten and the document version is incremented.
If the version number of the existing document is higher than the given version
the request is rejected as a conflict error. This conflict error is 
caught and the message is sent to the retry topic.

The syntax to define topic : retryTopic pairs is as follows:

```properties
kafka.producer.topics=topic1:retryTopci1,topic2:retryTopic2
```

## elastic.host

Host name of the elasticsearch cluster.

## elastic.port

Port of the elasticsearch cluster.