/*
 * elasticsearch consumer service
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.swissbib.linked

import org.apache.kafka.clients.producer.ProducerConfig
import org.apache.kafka.common.serialization.StringSerializer
import org.apache.logging.log4j.Logger
import org.swissbib.SbMetadataSerializer
import java.util.*
import kotlin.system.exitProcess

class ProducerPropertiesLoader(
        properties: ConsumerPropertiesLoader,
        private val log: Logger
) {
    private val appProperties: Properties = Properties()
    val kafkaProperties: Properties = Properties()

    init {
        val map = properties.appProperties + properties.kafkaProperties
        map.forEach {
            appProperties[it.key] = it.value
        }
        mapProperties()
    }

    private fun mapProperties() {
        setProperty(ProducerConfig.CLIENT_ID_CONFIG, abortIfMissing = true)
        setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, abortIfMissing = true)
        setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer::class.java)
        setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, SbMetadataSerializer::class.java)
        setProperty(ProducerConfig.BATCH_SIZE_CONFIG, 16384)
        setProperty(ProducerConfig.BUFFER_MEMORY_CONFIG, 33445532)
        setProperty(ProducerConfig.LINGER_MS_CONFIG, 1)
        setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, true)
        setProperty(ProducerConfig.ACKS_CONFIG)
        setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "gzip")
        setProperty(ProducerConfig.TRANSACTION_TIMEOUT_CONFIG)
        setProperty(ProducerConfig.TRANSACTIONAL_ID_CONFIG)
        setProperty(ProducerConfig.CONNECTIONS_MAX_IDLE_MS_CONFIG)
        setProperty(ProducerConfig.DELIVERY_TIMEOUT_MS_CONFIG)
        setProperty(ProducerConfig.SEND_BUFFER_CONFIG)
        setProperty(ProducerConfig.INTERCEPTOR_CLASSES_CONFIG)
        setProperty(ProducerConfig.RETRY_BACKOFF_MS_CONFIG)
        setProperty(ProducerConfig.RETRIES_CONFIG)
        setProperty(ProducerConfig.REQUEST_TIMEOUT_MS_CONFIG)
        setProperty(ProducerConfig.PARTITIONER_CLASS_CONFIG)
        setProperty(ProducerConfig.METRIC_REPORTER_CLASSES_CONFIG)
        setProperty(ProducerConfig.METRICS_NUM_SAMPLES_CONFIG)
        setProperty(ProducerConfig.METRICS_RECORDING_LEVEL_CONFIG)
        setProperty(ProducerConfig.METRICS_SAMPLE_WINDOW_MS_CONFIG)
        setProperty(ProducerConfig.MAX_BLOCK_MS_CONFIG)
        setProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION)
        setProperty(ProducerConfig.MAX_REQUEST_SIZE_CONFIG)
        setProperty(ProducerConfig.METADATA_MAX_AGE_CONFIG)
        setProperty(ProducerConfig.RECEIVE_BUFFER_CONFIG)
        setProperty(ProducerConfig.RECONNECT_BACKOFF_MAX_MS_CONFIG)
        setProperty(ProducerConfig.RECONNECT_BACKOFF_MS_CONFIG)
    }

    private fun setAppProperty(propertyName: String, defaultValue: Any? = null, abortIfMissing: Boolean = false) {
        val envProperty = propertyName.replace("\\.".toRegex(), "_").toUpperCase()
        when {
            System.getenv(envProperty) != null -> {
                log.debug("Found value for property $propertyName in environment variable $envProperty.")
                appProperties.setProperty(propertyName, System.getenv(envProperty))
            }
            appProperties.getProperty(propertyName) == null && defaultValue != null -> {
                log.debug("No value for $propertyName in appProperties file. Set $defaultValue from default.")
                appProperties[propertyName] = defaultValue
            }
            appProperties.getProperty(propertyName) != null -> {
                log.debug("Use property value from app.properties file: $propertyName=${appProperties.getProperty(propertyName)}")
            }
            abortIfMissing -> {
                log.error("Required property $propertyName not set! Aborting...")
                exitProcess(1)
            }
            else -> log.debug("No value for $propertyName set.")
        }
    }

    private fun setProperty(propertyName: String, defaultValue: Any? = null, abortIfMissing: Boolean = false) {
        val envProperty = propertyName.replace("\\.".toRegex(), "_").toUpperCase()
        when {
            System.getenv(envProperty) != null -> {
                log.trace("Found value for property $propertyName in environment variable $envProperty")
                kafkaProperties.setProperty(propertyName, System.getenv(envProperty))
            }
            appProperties.getProperty(propertyName) != null -> {
                log.trace("Found value for property $propertyName in appProperties file")
                kafkaProperties.setProperty(propertyName, appProperties.getProperty(propertyName))
                appProperties.remove(propertyName)
            }
            defaultValue != null -> {
                kafkaProperties[propertyName] = defaultValue
            }
            abortIfMissing -> {
                log.error("Required property $propertyName not set! Aborting...")
                exitProcess(1)
            }
            else -> log.trace("No value for property $propertyName found")
        }
    }
}
