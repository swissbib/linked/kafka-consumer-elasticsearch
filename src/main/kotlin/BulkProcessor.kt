/*
 * elasticsearch consumer service
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.http.HttpHost
import org.apache.kafka.clients.consumer.ConsumerRecord
import org.apache.logging.log4j.Logger
import org.elasticsearch.ElasticsearchException
import org.elasticsearch.action.bulk.BackoffPolicy
import org.elasticsearch.action.bulk.BulkProcessor
import org.elasticsearch.action.bulk.BulkRequest
import org.elasticsearch.action.bulk.BulkResponse
import org.elasticsearch.action.delete.DeleteRequest
import org.elasticsearch.action.update.UpdateRequest
import org.elasticsearch.client.RequestOptions
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.common.unit.ByteSizeUnit
import org.elasticsearch.common.unit.ByteSizeValue
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.common.xcontent.XContentType
import org.elasticsearch.rest.RestStatus
import org.swissbib.SbMetadataModel
import org.swissbib.types.EsBulkActions
import org.swissbib.types.EsMergeActions
import java.util.*
import kotlin.system.exitProcess

class BulkProcessor(private val consumer: Consumer,
                    private val producer: Producer,
                    private val properties: Properties,
                    private val log: Logger) {
    private val client = RestHighLevelClient(RestClient.builder(
            HttpHost(properties.getProperty("elastic.host"), properties.getProperty("elastic.port").toInt())))

    private fun getProducerTopic(topic: String): String
    {
        val result = properties.getProperty("kafka.producer.$topic")
        if (result != null) {
            return result
        } else {
            log.error("No producer topic for $topic. (Add 'kafka.producer.$topic=??? to app.properties)")
            exitProcess(1)
        }
    }


    private val listener = object : BulkProcessor.Listener {
        override fun beforeBulk(executionId: Long, request: BulkRequest) {
            log.trace("Preparing bulk upload {}", executionId)
        }

        override fun afterBulk(executionId: Long, request: BulkRequest, response: BulkResponse) {
            if (response.hasFailures()) {
                val responses = response.items
                val requests = request.requests()
                for (i in 0 until request.numberOfActions()) {
                    val responseItem = responses[i]
                    if (responseItem.isFailed) {
                        log.error("{} - {}", responseItem.failure.status.status, responseItem.failureMessage)
                        if (responseItem.failure.status == RestStatus.CONFLICT) {
                            if (requests[i] is ConsumerRecordIndexRequest) {
                                val consumerRecordIndexRequest = requests[i] as ConsumerRecordIndexRequest
                                val sourceTopic = consumerRecordIndexRequest.consumerRecord.topic()
                                producer.send(getProducerTopic(sourceTopic),
                                        consumerRecordIndexRequest.consumerRecord.key(),
                                        SbMetadataModel()
                                                .setData(consumerRecordIndexRequest.metadataModel.data)
                                                .setEsMergeAction(EsMergeActions.RETRY)
                                                .setEsDocumentPrimaryTerm(consumerRecordIndexRequest.ifPrimaryTerm())
                                                .setEsDocumentSeqNum(consumerRecordIndexRequest.ifSeqNo())
                                )
                                log.warn("Message with id {} was rejected with version conflict and was resent to {}.", responses[i].id, getProducerTopic(sourceTopic))
                            }
                        } else if (responseItem.failure.cause is ElasticsearchException) {
                            if (responseItem.failure.status != RestStatus.FORBIDDEN) {
                                if (requests[i] is ConsumerRecordIndexRequest) {
                                    val consumerRecordIndexRequest = requests[i] as ConsumerRecordIndexRequest
                                    log.error("Failed to index the following body: ${consumerRecordIndexRequest.metadataModel.data}")
                                }
                            }
                            log.error("{}", responseItem.failure)
                        } else {
                            if (requests[i] is ConsumerRecordIndexRequest) {
                                val consumerRecordIndexRequest = requests[i] as ConsumerRecordIndexRequest
                                log.error("Failed to index the following body: ${consumerRecordIndexRequest.metadataModel.data}")
                            }
                            log.error("Message with id {} was rejected because of unknown error: {}", responses[i].id, responses[i].failureMessage)
                        }
                    }
                }
            }
            log.info("Bulk upload {} successful. Indexed {} messages. Commit offsets to Kafka. Any failures are resent for processing.", executionId,
                    request.numberOfActions())
        }

        override fun afterBulk(executionId: Long, request: BulkRequest, failure: Throwable) {
            val message = failure.message
            if (message != null) {
                if (message.startsWith("Unable to parse response body for")) {
                    log.info("Could not parse response body for bulk upload {}: {}", executionId, message)
                    return
                }
            }
            log.error("Bulk upload {} ended in a failure: {}. This failed {} actions.",
                    executionId, message, request.numberOfActions())
            for (item in request.requests()) {
                log.error("Failed to index: {}", item.id())
            }
            for (t in failure.suppressed) {
                log.error("Suppressed Error: {}", t.message)
            }

        }
    }

    private val bulk = processor()

    private fun processor(): BulkProcessor {
        val builder = BulkProcessor.builder(
                { request, bulkListener -> client.bulkAsync(request, RequestOptions.DEFAULT, bulkListener) }, listener)
        // Create a bulk request when there are 10'000 requests, 5 MB request data or 10 min have passed since last
        // request.
        builder.setBulkActions(10000)
        builder.setBulkSize(ByteSizeValue(5, ByteSizeUnit.MB))
        builder.setFlushInterval(TimeValue.timeValueMinutes(10L))
        builder.setBackoffPolicy(BackoffPolicy
                .constantBackoff(TimeValue.timeValueSeconds(1L), 3))
        // important as otherwise there is a ConcurrentModificationException on the KafkaConsumer. The KafkaConsumer is not
        // Thread Safe!
        builder.setConcurrentRequests(0)
        return builder.build()
    }


    fun process() {
        val records = consumer.consume()
        records.forEach {
            add(it)
        }
    }

    private fun add(consumerRecord: ConsumerRecord<String, SbMetadataModel>) {
        try {
            when (consumerRecord.value().esBulkAction) {
                EsBulkActions.INDEX -> {
                    val recordIndexRequest = ConsumerRecordIndexRequest(consumerRecord)
                    bulk.add(recordIndexRequest)
                }
                EsBulkActions.DELETE -> bulk.add(DeleteRequest(
                        consumerRecord.value().esIndexName,
                        consumerRecord.key()))
                EsBulkActions.CREATE -> {
                    val recordIndexRequest = ConsumerRecordIndexRequest(consumerRecord)
                    recordIndexRequest.create(true)
                    bulk.add(recordIndexRequest)
                }
                EsBulkActions.UPDATE -> {
                    val recordUpdateRequest = UpdateRequest(consumerRecord.value().esIndexName, consumerRecord.key())
                    recordUpdateRequest.doc(consumerRecord.value().data, XContentType.JSON)
                    bulk.add(recordUpdateRequest)
                }
                else -> log.error("Could not index message as no Es Bulk Action was specified from topic " + consumerRecord.topic() + ".")
            }
        } catch (ex: NullPointerException) {
            log.error(ex.message, consumerRecord.key())
        }
    }
}