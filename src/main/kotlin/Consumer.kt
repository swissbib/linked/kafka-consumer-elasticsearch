/*
 * elasticsearch consumer service
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib.linked

import org.apache.kafka.clients.consumer.ConsumerRecords
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.swissbib.SbMetadataModel
import java.time.Duration
import java.util.*

class Consumer(private val appProperties: Properties, properties: Properties) {
    private val instance = KafkaConsumer<String, SbMetadataModel>(properties)

    init {
        instance.subscribe(appProperties.getProperty("kafka.consumer.topics").split(','))
    }

    fun consume(): ConsumerRecords<String, SbMetadataModel> {
        return instance.poll(Duration.ofMillis(appProperties.getProperty("kafka.consumer.poll").toLong()))
    }



}